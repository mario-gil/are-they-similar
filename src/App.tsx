import * as React from "react";
import "./App.css";
import CollectionComparison from "./CollectionComparison";

import logo from "./logo.svg";

const App = () => (
  <div className="App">
    <header className="App-header">
      <img src={logo} className="App-logo" alt="logo" />
      <h1 className="App-title">Are they similar?</h1>
    </header>
    <p className="App-intro">
      We consider collections to be similar if one of their permutations
      resulting from a single swap between two of their elements are equal
    </p>
    <CollectionComparison />
  </div>
);

export default App;
