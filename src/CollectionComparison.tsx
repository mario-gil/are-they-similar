import * as React from "react";
import "./CollectionComparison.css";
import Input from "./Input";
import compareArrays from "./util/compareArrays";
import hasAnyElementInCommon from "./util/hasAnyElementInCommon";
import getOneStepPermutations from "./util/oneStepPermutations";

interface ICollection {
  value: string[];
  permutations: string[][];
}
interface IDictionary<T> {
  [name: string]: T;
}
type State = Readonly<typeof initialState>;
const initialState = {
  collections: {} as IDictionary<ICollection>,
  names: ["Collection A", "Collection B"]
};

export default class CollectionComparison extends React.Component<
  object,
  State
> {
  private static renderPermutations<T>(permutations: string[][], name: string) {
    return permutations.map((permutation, i) => (
      <li key={`${name}-${i}`}>{permutation}</li>
    ));
  }
  public readonly state: State = initialState;

  private getPermutations = getOneStepPermutations;
  private areSimilar = hasAnyElementInCommon(compareArrays);

  public render() {
    const { collections, names } = this.state;
    const collectionList = names
      .filter(name => !!collections[name])
      .map(name => collections[name]);
    const permutations = collectionList.map(
      collection => collection.permutations
    );
    const areSimilar =
      permutations[0] && permutations[1]
        ? this.areSimilar(permutations[0], permutations[1])
        : false;
    const similarText = areSimilar ? "Similar" : "Not Similar";
    return (
      <div>
        <h2>{similarText}</h2>
        {this.renderCollections(this.state)}
      </div>
    );
  }

  private handleTextInput = (event: React.SyntheticEvent, name: string) => {
    const collection = (event.target as HTMLInputElement).value.split("");
    const permutations = this.getPermutations(collection);
    this.setState(this.updateValue(name, collection));
    this.setState(this.updatePermutations(name, permutations));
  };

  private updateValue<T>(name: string, value: string[]) {
    return (prevState: State): State => ({
      ...prevState,
      collections: {
        ...prevState.collections,
        [name]: {
          ...prevState.collections[name],
          value
        }
      }
    });
  }

  private updatePermutations<T>(name: string, permutations: string[][]) {
    return (prevState: State): State => ({
      ...prevState,
      collections: {
        ...prevState.collections,
        [name]: {
          ...prevState.collections[name],
          permutations
        }
      }
    });
  }

  private renderCollections = (state: State) => {
    const { collections, names } = state;
    return names.map(name => this.renderCollection(name, collections[name]));
  };

  private renderCollection = (name: string, collection: ICollection) => {
    const permutations = collection ? collection.permutations : [];
    const hasPermutations = permutations.length > 0;
    return (
      <div key={name} className={"CollectionComparison"}>
        <label className={"Text-label"}>
          <strong>{name}</strong>
        </label>
        <Input onChange={this.handleTextInput} name={name} />
        {hasPermutations && (
          <ul>{CollectionComparison.renderPermutations(permutations, name)}</ul>
        )}
      </div>
    );
  };
}
