import * as React from "react";

interface IInput {
  name: string;
  onChange: (data: React.SyntheticEvent, name: string) => void;
}

const curry = <T, U, X>(fn: (t: T, u: U) => X, curriedArg: U) => (arg: T) =>
  fn(arg, curriedArg);

const Input: React.SFC<IInput> = props => {
  const callWithName = curry(props.onChange, props.name);
  const { name } = props;
  return <input type="text" name={name} onChange={callWithName} />;
};

export default Input;
