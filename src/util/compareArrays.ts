function compareArrays<T>(collection: T[], otherCollection: T[]) {
  if (!(collection.length === otherCollection.length)) {
    return false;
  }
  return collection.every((item, i) => item === otherCollection[i]);
}

export default compareArrays;