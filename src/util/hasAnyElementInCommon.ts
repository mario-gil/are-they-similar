const hasAnyElementInCommon = <T>(equals: (a: T, b: T) => boolean) => {
  return (collection: T[], otherCollection: T[]) => {
    return collection.some(item => {
      return otherCollection.some(otherItem => equals(item, otherItem));
    });
  };
};

export default hasAnyElementInCommon;
