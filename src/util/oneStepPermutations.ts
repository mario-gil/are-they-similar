import range from "./range";
import swap from "./swap";

export const swapWithAllFromIndex = (index: number) => <T>(collection: T[]): T[][] => {
  const nextIndex = index + 1;
  const nextIndexes = range(nextIndex, collection.length);
  return nextIndexes.map(next => swap(collection, index, next));
};

type Permutator = (index: number) => <T>(collection: T[]) => T[][];

const getPermutations = (permutate: Permutator) => <T>(
  collection: T[]
): T[][] => {
  const permutations: T[][][] = collection.map((x, i) => permutate(i)(collection));
  const withOriginalPermutation = permutations.concat([[collection]]);
  return [].concat.apply([], withOriginalPermutation);
};

const getOneStepPermutations = getPermutations(swapWithAllFromIndex);

export default getOneStepPermutations;
