const range = (lowerBound: number, upperBound: number) => {
  const size = upperBound - lowerBound;
  return Array.from(new Array(size), (_, i) => i + lowerBound);
};

export default range;