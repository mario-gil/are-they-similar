const boundsCheck = (min: number) => (max: number) => (n: number): boolean =>
  n >= min && n < max;
const boundsCheckFromZeroTo = boundsCheck(0);

function swap(
  collection: any[],
  index: number,
  otherIndex: number
): any[] {
  const length = collection.length;
  const isInLengthBounds = boundsCheckFromZeroTo(length);
  if (!(isInLengthBounds(index) && isInLengthBounds(otherIndex))) {
    throw new RangeError("An index is out of bounds");
  }

  const copyCollection = [...collection];
  copyCollection[index] = collection[otherIndex];
  copyCollection[otherIndex] = collection[index];
  return copyCollection;
}

export default swap;
