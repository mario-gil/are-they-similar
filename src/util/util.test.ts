import getOneStepPermutations, {
  swapWithAllFromIndex
} from "./oneStepPermutations";
import range from "./range";
import swap from "./swap";

const collection = [1, 2, 3, 4, 5];
const shorterCollection = collection.slice(0, 3);
const swappedCollection = [2, 1, 3, 4, 5];

describe("The swap function", () => {
  describe("when given valid parameters", () => {
    it("gives a swapped list", () => {
      expect(swap(collection, 0, 1)).toEqual(swappedCollection);
    });
  });

  describe("when an index is out of bounds", () => {
    it("throws an exception", () => {
      expect(() => swap(collection, 0, 100)).toThrow(RangeError);
    });
  });
});

describe("The range function", () => {
  describe("when given valid lower and upper bounds", () => {
    it("gives a natural range from lower to upper bound (not inclusive)", () => {
      const copyCollection = [...collection];
      copyCollection.pop();
      expect(range(1, 5)).toEqual(copyCollection);
    });
  });
});

describe("The single swap permutator function", () => {
  describe("when given a valid collection", () => {
    it("gives a list with the single swap permutations", () => {
      const oneStepPermutationsFromFirst = [[2, 1, 3], [3, 2, 1]];
      const oneStepPermutationsFromSecond = [[1, 3, 2]];
      const oneStepPermutationsFromThird: number[] = [];
      expect(swapWithAllFromIndex(0)(shorterCollection)).toEqual(
        oneStepPermutationsFromFirst
      );
      expect(swapWithAllFromIndex(1)(shorterCollection)).toEqual(
        oneStepPermutationsFromSecond
      );
      expect(swapWithAllFromIndex(2)(shorterCollection)).toEqual(
        oneStepPermutationsFromThird
      );
    });
  });
});

describe("The single permutations function", () => {
  describe("when given a valid list", () => {
    it("gives a list with the possible unique permutations for each number", () => {
      const oneStepPermutations = [[2, 1, 3], [3, 2, 1], [1, 3, 2], [1, 2, 3]];
      expect(getOneStepPermutations(shorterCollection)).toEqual(
        oneStepPermutations
      );
    });
  });
});
