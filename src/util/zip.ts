const zip = <T, U>(collection: T[], otherCollection: U[]) =>
  collection.map((x, i) => [x, otherCollection[i]]);

export default zip;